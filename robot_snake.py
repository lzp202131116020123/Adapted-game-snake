from snake import Snake
import random
import math

class RobotSnake(Snake):
    #继承玩家蛇
    def __init__(self, game):
        super().__init__(game)
        self.color = (255,255,255)
        self.hp = 100000000

        self.random_turn_interval = 5  # 随机转向的间隔
        self.turn_counter = 0
        for _ in range(5):  # 假设长度
            self.grow()

    def random_move(self):
        # 在每隔一定时间间隔内随机改变方向
        if self.turn_counter >= self.random_turn_interval:
            random_angle = random.uniform(-math.pi, math.pi)
            self.set_direction(self.direction + random_angle)
            self.turn_counter = 0
        else:
            self.turn_counter += 1

    def update(self):

        #展示由segments段组成的机器蛇
        for i in range(self.length):
            self.segments[i].set_width(self.seg_width[i])
            self.segments[i].update()

        del self.poly_points[:]
        for seg in self.segments:
            self.poly_points.append(seg.get_point1(self.game.get_offset()))
        for i in range(self.length - 1, -1, -1):
            self.poly_points.append(self.segments[i].get_point2(self.game.get_offset()))
        # 计算与玩家蛇头部的距离
        player_head_pos = self.game.snake.segments[0].center
        robot_head_pos = self.segments[0].center
        distance = math.hypot(player_head_pos[0] - robot_head_pos[0], player_head_pos[1] - robot_head_pos[1])

        # 设定最大距离和最小距离阈值，这里可以调节机器蛇的干扰程度（即抢食速度）
        max_distance = 500
        min_distance = 20

        if distance > max_distance:
            # 如果距离太远，调整方向朝向玩家蛇
            direction_to_player = math.atan2(player_head_pos[1] - robot_head_pos[1], player_head_pos[0] - robot_head_pos[0])
            self.set_direction(direction_to_player)
        elif distance < min_distance:
            # 如果距离太近，随机选择一个新的方向远离玩家蛇
            angle_away_from_player = math.atan2(robot_head_pos[1] - player_head_pos[1], robot_head_pos[0] - player_head_pos[0])
            new_direction = angle_away_from_player + random.uniform(-math.pi/4, math.pi/4)
            self.set_direction(new_direction)

        # 继续执行随机移动逻辑
        self.random_move()

    # 绘制方法使用父类的绘制方法
